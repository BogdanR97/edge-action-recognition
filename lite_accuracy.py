import argparse
import time
import tensorflow as tf
import numpy as np

from data_fetcher import DataFetcher
from preprocessor import Preprocessor

def step_decay(epoch, lr):
    if epoch != 0 and epoch % 50 == 0:
        lr = lr / 10
    return lr

def main():

    IMG_HEIGHT = 240
    IMG_WIDTH = 320
    BATCH_SIZE = 64

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--dataset', help='Path to the dataset directory', required=True)
    parser.add_argument(
        '--lite_model', help='Path to the lite_model', required=True)
    parser.add_argument(
        '--split', help='The split on which the model was trained', required=True)
    args = parser.parse_args()

    df = DataFetcher(args.dataset)
    pp = Preprocessor(IMG_HEIGHT, IMG_WIDTH, BATCH_SIZE)

    split_id = int(args.split)

    with tf.io.gfile.GFile(args.lite_model, 'rb') as f:
        model_content = f.read()

    interpreter = tf.lite.Interpreter(model_content=model_content)
    interpreter.allocate_tensors()
    input_index = interpreter.get_input_details()[0]['index']
    output = interpreter.tensor(interpreter.get_output_details()[0]["index"])

    _, validation_set = df.get_split(split_id)
    validation_set = [pp.clean_and_scale(data) for data in validation_set]

    x_set = [data['X'].transpose() for data in validation_set]
    y_set = [data['Y'] for data in validation_set]

    accurate_count = 0

    for (X, Y) in zip(x_set, y_set):
        frames = X
        frames = tf.expand_dims(frames, 0).numpy()
        frames = tf.cast(frames, tf.float32)
        gt_action = Y

        interpreter.set_tensor(input_index, frames)
        interpreter.invoke()

        predict_action = np.argmax(output()[0])

        accurate_count += (predict_action == gt_action)

    accuracy = accurate_count * 1.0 / len(validation_set)
    print('TensorFlow Lite model accuracy = %.6f' % accuracy)


if __name__ == "__main__":
    main()
    