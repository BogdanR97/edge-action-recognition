# Edge Action Recognition

The root folder contains:
 1. Python scripts for training the action-recognition model.
 2. 'dataset' folder - it contains training and validation data
    split into action categories folders and the 'actions.txt' file.
 3. edge_action folder - Python scripts and models for Raspberry Pi inference

Prerequisites for training the model:
1. Python 3.6.9
2. Tensorflow 2.0
3. NumPy
4. SciPy

Prerequisites for executing inference on the Rasperry Pi:
1. Python 3.7
2. TensorFlow Runtime Package (https://www.tensorflow.org/lite/guide/python)
3. OpenCV (cv2 package for python)
4. NumPy
5. picamera package
    

The workflow for obtaining a model for Pi inference is the following:

1. While in the root folder go in the 'dataset' folder.

2. Fill up the 'actions.txt' file with the actions that the model should
learn to recognize. One action per line. Actions must have the same name
as the action folders' names.

3. Go back to the root folder.

4. Excute python script 'run.py'. A valid command looks like this:
`python3 run.py --dataset /dataset --model_save models_save_directory`
where the first argument is the path to the dataset and the second argument
is the path to a folder where the models will be saved. The training process
will start.

5. After the training process has finished, the script will have outputted 3 models.
Each of the three models is placed in one of the folders '1', '2', or '3'.
Each model's accuracy is reported during the training at stdout.

6. Convert the model with the best accuracy using the 'converter.py' script:
`python3 converter.py --model models_save_directory/2 --save_path ./best_model.tflite`
The first argument is the path to a saved model directory (in the example,
the second model is used, and thus the '2') and the second argument is the 
path to a file where the converted model will be saved.

7. Transfer the file 'best_model.tflite' to the Raspberry Pi.

For Pi inference, follow these steps:

1. Transfer the 'edge_action' folder contents to a Raspberry Pi device with a 
Pi Camera attached to it. Also, make sure that the previous obtained model
has been transferred successfully.

2. Make sure that the same 'actions.txt' file used for training the model is
present on the Raspberry as well.

3. Execute the 'picamera_stream.py' script: the camera will start recording
and after a few seconds the action predictions will appear at stdout. A valid
command looks like this:
`python3 picamera_stream.py --pose_model posenet_mobilenet_v1_100_257x257_multi_kpt_stripped.tflite --action_model edge_action_model.tflite --actions actions.txt --print_pose Y`
Arguments, explained in order:
    1. pose_model: path to th PoseNet model. The PoseNet model file is already
    present in the 'edge_action' folder.
    2. action_model: path to the converted action-recognition model.
    An example is present in the 'edge_action' folder.
    3. actions: path to the 'actions.txt' file
    4. print_pose: if Y is provided, during inference a window depicting the
    picamera's video stream will be displayed on screen.
    
    