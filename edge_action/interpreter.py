from tflite_runtime.interpreter import Interpreter

class ModelInterpreter():

    def __init__(self, model):
        self.model_interpreter = Interpreter(model)
        self.model_interpreter.allocate_tensors()

    def get_input_size(self):
        return self.model_interpreter.get_input_details()[0]['shape']

    def set_input_tensor(self, input_data):
        tensor_index = self.model_interpreter.get_input_details()[0]['index']
        input_tensor = self.model_interpreter.tensor(tensor_index)()[0]
        input_tensor[:, :] = input_data
    
    def get_output_details(self, input_data):
        self.set_input_tensor(input_data)
        self.model_interpreter.invoke()
        
        return self.model_interpreter.get_output_details()