import picamera
import picamera.array
import argparse
import time
import numpy as np
import cv2
from imutils.video import FPS
from constants import JOINT_ORDER
from constants import DISPLAY_HEIGHT, DISPLAY_WIDTH

from pose_interpreter import PoseInterpreter
from action_interpreter import ActionInterpreter

def add_frame(joints_img, keypoints):
    frame = np.zeros(shape=(3, 13), dtype=np.float32)
    for i, part in enumerate(JOINT_ORDER):
        keypoint = [k for k in keypoints if k['part'] == part].pop()

        frame[0][i] = keypoint['position']['x']
        frame[1][i] = keypoint['position']['y']

    if joints_img.size == 0:
        return frame
    else:
        return np.dstack((joints_img, frame))

def init_load_frame(camera, pi_rgb_array, num_frames, pose_interpreter, frame_width, frame_height):
    joints_img = np.array([])

    for i, frame in enumerate(camera.capture_continuous(pi_rgb_array,
                                                format='rgb', 
                                                use_video_port=True)):
        resized_frame = cv2.resize(frame.array, dsize=(frame_width, frame_height), interpolation=cv2.INTER_LINEAR)
        keypoints, _ = pose_interpreter.get_pose(resized_frame)
        joints_img = add_frame(joints_img, keypoints)

        pi_rgb_array.truncate(0)

        if i >= num_frames - 1:
            return joints_img

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--pose_model', help='File path of pose .tflite file.', required=True)
    parser.add_argument(
        '--action_model', help='File path of action .tflite file.', required=True)
    parser.add_argument(
        '--actions', help='File containing recognizable actions.', required=True)
    parser.add_argument(
        '--print_pose', help='Type "Y" or "N" for pose output', required=False)

    args = parser.parse_args()

    pose_interpreter = PoseInterpreter(args.pose_model)
    frame_height, frame_width = pose_interpreter.get_input_img_sizes()

    action_interpreter = ActionInterpreter(args.action_model, args.actions)
    num_frames, num_joints = action_interpreter.get_input_joints_img_sizes()

    print_pose = False  
    if args.print_pose == 'Y': print_pose = True

    joints_img = np.array([])

    with picamera.PiCamera() as camera:
        frame_res = (DISPLAY_WIDTH, DISPLAY_HEIGHT)
        camera.resolution = frame_res
        pi_rgb_array = picamera.array.PiRGBArray(camera, size=frame_res)
        time.sleep(2)

        joints_img = init_load_frame(camera, 
                                    pi_rgb_array, 
                                    num_frames, 
                                    pose_interpreter,
                                    frame_width,
                                    frame_height)

        for frame in camera.capture_continuous(pi_rgb_array,
                                                format='rgb', 
                                                use_video_port=True):
            resized_frame = cv2.resize(frame.array, dsize=(frame_width, frame_height), interpolation=cv2.INTER_LINEAR)
            keypoints, total_score = pose_interpreter.get_pose(resized_frame)
            pi_rgb_array.truncate(0)

            if print_pose: pose_interpreter.print_pose(keypoints, frame.array, 1)

            joints_img = add_frame(joints_img[:, :, 1:], keypoints)
            action_name, action_score = action_interpreter.get_action(np.copy(joints_img))
            print(action_name + " - " + str(action_score))

if __name__ == "__main__":
    main()

