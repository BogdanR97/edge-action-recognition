from interpreter import ModelInterpreter
import numpy as np
from constants import ACTION_NAMES
from constants import PREDICTION_TRESHOLD

class ActionInterpreter(ModelInterpreter):

    def __init__(self, model, actions_file):
        super().__init__(model)
        _, self.num_frames, self.num_joints, _ = self.get_input_size()

        with open(actions_file) as f:
            temp = f.read()
            actions = temp.split('\n')
            self.action_name = [action for action in actions if action != '']

    def get_input_joints_img_sizes(self):
        return self.num_frames, self.num_joints
    
    def get_action(self, joints_img):
        joints_img = self.normalize_input(joints_img)
        joints_img = joints_img.transpose()
        joints_img = np.expand_dims(joints_img, axis=0)

        output_details = self.get_output_details(joints_img)
        action_output = np.squeeze(self.model_interpreter.get_tensor(output_details[0]['index']))

        action_score = np.max(action_output)
        action_name = self.action_name[np.argmax(action_output)]

        if action_score < PREDICTION_TRESHOLD:
            action_name = "no action"

        return action_name, action_score

    def normalize_input(self, joints_img):
        orig_img = np.copy(joints_img)

        curr_min_x = np.min(joints_img[0, :, :][joints_img[0, :, :] > 0])
        curr_min_y = np.min(joints_img[1, :, :][joints_img[1, :, :] > 0])

        joints_img[0, :, :] = joints_img[0, :, :] - curr_min_x
        joints_img[1, :, :] = joints_img[1, :, :] - curr_min_y

        max_factor_x = 1 / np.max(joints_img[0, :, :])
        max_factor_y = 1 / np.max(joints_img[1, :, :])

        joints_img[0, :, :] = joints_img[0, :, :] * max_factor_x
        joints_img[1, :, :] = joints_img[1, :, :] * max_factor_y
        joints_img[0, :, :][orig_img[0, :, :] == 0] = 0
        joints_img[1, :, :][orig_img[1, :, :] == 0] = 0

        return joints_img