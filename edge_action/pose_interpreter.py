from interpreter import ModelInterpreter
import numpy as np
import cv2
from constants import OUTPUT_STRIDE, NUM_KEYPOINTS, JOINT_NAMES
from constants import DISPLAY_HEIGHT, DISPLAY_WIDTH
from constants import CONFIDENCE_TRESHOLD

class PoseInterpreter(ModelInterpreter):

    def __init__(self, model):
        super().__init__(model)
        _, self.height, self.width, _ = self.get_input_size()

    def get_input_img_sizes(self):
        return self.height, self.width

    def get_pose(self, image):
        image = self.normalize_input(image)
        
        output_details = self.get_output_details(image)

        tensors_index = [output['index'] for output in output_details]
        tensors = [self.model_interpreter.get_tensor(i) for i in tensors_index]
        heatmap_scores, offsets, _, _ = tensors

        # Using sigmoid function to scale all values in the range (-1, 1)
        heatmap_scores = self.sigmoid_func(heatmap_scores)
        heatmap_coords = self.argmax2d(heatmap_scores)

        offset_points = self.get_offset_points(heatmap_coords, offsets)
        confidence_keypoints = self.get_confidence_points(heatmap_scores, heatmap_coords)

        keypoints = []
        total_score = 0

        for keypoint_id in range(NUM_KEYPOINTS):

            
            total_score += confidence_keypoints[keypoint_id]

            if confidence_keypoints[keypoint_id] < CONFIDENCE_TRESHOLD:
                offset_points[keypoint_id][0] = 0
                offset_points[keypoint_id][1] = 0

            keypoint = {
                'position': {
                    'y': offset_points[keypoint_id][0],
                    'x': offset_points[keypoint_id][1]
                },
                'part': JOINT_NAMES [keypoint_id],
                'score': confidence_keypoints[keypoint_id]
            }

            keypoints.append(keypoint)

        return keypoints, total_score / NUM_KEYPOINTS

    def normalize_input(self, image):
        normalized_pixels = np.subtract(np.divide(image, 127.5), 1.0)
        normalized_pixels = np.expand_dims(normalized_pixels, axis=0)
        
        return normalized_pixels

    def sigmoid_func(self, mat):
        return np.divide(1.0, (1.0 + np.exp(-mat)))

    def argmax2d(self, raw_prediction):
        _, height, width, depth = raw_prediction.shape

        reshaped = np.reshape(raw_prediction, (height * width, depth))
        coords = np.argmax(reshaped, axis=0)

        y_coords = np.expand_dims(np.int32(np.divide(coords, width)), axis=1)
        x_coords = np.expand_dims(np.mod(coords, width), axis=1)

        return np.concatenate((y_coords, x_coords), axis=1)

    def get_offset_points(self, heatmap_coords, offsets):
        offset_vectors = np.array([])

        for keypoint_ind in range(NUM_KEYPOINTS):
            keypoint_y = heatmap_coords[keypoint_ind][0]
            keypoint_x = heatmap_coords[keypoint_ind][1]

            offset_y = offsets[0][keypoint_y][keypoint_x][keypoint_ind]
            offset_x = offsets[0][keypoint_y][keypoint_x][keypoint_ind + NUM_KEYPOINTS]

            offset_vectors = np.append(offset_vectors, [offset_y, offset_x])

        offset_vectors = np.reshape(offset_vectors, (-1, 2))

        # Introducing STRIDE
        offset_points = np.add(np.multiply(heatmap_coords, OUTPUT_STRIDE), offset_vectors)

        return offset_points

    def get_confidence_points(self, heatmap_scores, heatmap_coords):
        confidence_list = np.array([])

        for keypoint_ind in range(NUM_KEYPOINTS):
            keypoint_y = heatmap_coords[keypoint_ind][0]
            keypoint_x = heatmap_coords[keypoint_ind][1]

            confidence_list = np.append(confidence_list, heatmap_scores[0][keypoint_y][keypoint_x][keypoint_ind])

        return confidence_list

    def print_pose(self, keypoints, raw_image, fps):
        image = raw_image

        # Radius of circle 
        radius = 3
            
        # Blue color in BGR 
        color = (255, 0, 0) 
            
        # Line thickness
        thickness = -1
            
        for keypoint in keypoints:
            if int(keypoint['position']['x']) != 0 and int(keypoint['position']['y']) != 0:
                center_coordinates = self.scale_point(keypoint)
                image = cv2.circle(image, center_coordinates, radius, color, thickness)

        cv2.imshow("image", cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
        cv2.waitKey(fps)

    def scale_point(self, keypoint):
        new_x = int(keypoint['position']['x'] * (DISPLAY_WIDTH / self.width))
        new_y = int(keypoint['position']['y'] * (DISPLAY_HEIGHT / self.height))

        return new_x, new_y