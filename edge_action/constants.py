JOINT_NAMES = [
    'nose', 'leftEye', 'rightEye', 'leftEar', 'rightEar', 'leftShoulder',
    'rightShoulder', 'leftElbow', 'rightElbow', 'leftWrist', 'rightWrist',
    'leftHip', 'rightHip', 'leftKnee', 'rightKnee', 'leftAnkle', 'rightAnkle'
]

JOINT_ORDER = [
    'nose', 'rightShoulder', 'leftShoulder', 'rightHip', 'leftHip',
    'rightElbow', 'leftElbow', 'rightKnee', 'leftKnee', 'rightWrist', 
    'leftWrist', 'rightAnkle', 'leftAnkle'
]

ACTION_NAMES = [
    "brush_hair", "catch", "clap", "climb_stairs", "golf", "jump",
    "kick_ball", "pick", "pour", "pullup", "push", "run", "shoot_ball",
    "shoot_bow", "shoot_gun", "sit", "stand", "swing_baseball",
    "throw", "walk", "wave",
]

NUM_KEYPOINTS = 17
OUTPUT_STRIDE = 32

DISPLAY_WIDTH = 640
DISPLAY_HEIGHT = 480

CONFIDENCE_TRESHOLD = 0.5
PREDICTION_TRESHOLD = 0.5
