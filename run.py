import argparse
import time
from statistics import mean

from data_fetcher import DataFetcher
from preprocessor import Preprocessor
from model import Model

def step_decay(epoch, lr):
    if epoch != 0 and epoch % 30 == 0:
        lr = lr / 10
    return lr

def main():

    IMG_HEIGHT = 240
    IMG_WIDTH = 320
    BATCH_SIZE = 64

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--dataset', help='Path to the dataset directory', required=True)
    parser.add_argument(
        '--model_save', help='Path to the directory where the model will be saved', required=True)
    args = parser.parse_args()

    df = DataFetcher(args.dataset)
    pp = Preprocessor(IMG_HEIGHT, IMG_WIDTH, BATCH_SIZE)
    models_acc = []
    
    for i in range(1, 4):
        start_time = time.time()
        train_set, validation_set = df.get_split(i)

        train_set = [pp.clean_and_scale(data) for data in train_set]
        validation_set = [pp.clean_and_scale(data) for data in validation_set]

        train_set = pp.batch(train_set)
        validation_set = pp.batch(validation_set, train=False)

        model = Model(step_decay, df.get_actions_no())
        _, test_acc = model.fit(train_set, validation_set)
        models_acc.append(test_acc)

        elapsed_time = time.time() - start_time
        print("Elapsed time: " + str(elapsed_time) + " seconds\n")

        model.save(args.model_save + '/' + str(i))
    
    best_set_acc = max(models_acc)
    best_set_index = models_acc.index(best_set_acc) + 1
    print("Best set was " + str(best_set_index) + ": {:.3%}\n".format(best_set_acc))

    print("Overall accuracy was: {:.3%}.".format(sum(models_acc) / len(models_acc)))

if __name__ == "__main__":
    main()
    