import tensorflow as tf
import numpy as np
import random
import os
import re

class Preprocessor():

    def __init__(self, img_height, img_width, batch_size):
        self.img_height = img_height
        self.img_width = img_width
        self.batch_size = batch_size

    def remove_joints_outside_img(self, joints_img):
        orig_img = np.copy(joints_img)
        img_height = self.img_height
        img_width = self.img_width

        joints_img[0, :, :][orig_img[0, :, :] > img_width] = 0
        joints_img[0, :, :][orig_img[0, :, :] < 0] = 0
        joints_img[1, :, :][orig_img[0, :, :] > img_width] = 0
        joints_img[1, :, :][orig_img[0, :, :] < 0] = 0
        joints_img[0, :, :][orig_img[1, :, :] > img_height] = 0
        joints_img[0, :, :][orig_img[1, :, :] < 0] = 0
        joints_img[1, :, :][orig_img[1, :, :] > img_height] = 0
        joints_img[1, :, :][orig_img[1, :, :] < 0] = 0

        return joints_img

    def scale_joints_coords(self, joints_img):
        orig_img = np.copy(joints_img)
        img_height = self.img_height
        img_width = self.img_width

        curr_min_x = np.min(joints_img[0, :, :][joints_img[0, :, :] > 0])
        curr_min_y = np.min(joints_img[1, :, :][joints_img[1, :, :] > 0])
        curr_max_x = np.max(joints_img[0, :, :])
        curr_max_y = np.max(joints_img[1, :, :])

        max_factor_x = img_width / curr_max_x
        max_factor_y = img_height / curr_max_y
        min_factor_x = (img_width * 0.1) / (curr_max_x - curr_min_x)
        min_factor_y = (img_height * 0.1) / (curr_max_y - curr_min_y)

        min_factor = max(min_factor_x, min_factor_y)
        max_factor = min(max_factor_x, max_factor_y)
        factor = random.uniform(min_factor, max_factor)

        joints_img[0, :, :] = joints_img[0, :, :] * factor
        joints_img[1, :, :] = joints_img[1, :, :] * factor
        joints_img[0, :, :][orig_img[0, :, :] == 0] = 0
        joints_img[1, :, :][orig_img[1, :, :] == 0] = 0

        return joints_img

    def translate_joints_coords(self, joints_img):
        orig_img = np.copy(joints_img)
        img_height = self.img_height
        img_width = self.img_width
        
        max_minus_translate_x = -np.min(joints_img[0, :, :][joints_img[0, :, :] > 0])
        max_minus_translate_y = -np.min(joints_img[1, :, :][joints_img[1, :, :] > 0])
        max_plus_translate_x = img_width - np.max(joints_img[0, :, :])
        max_plus_translate_y = img_height - np.max(joints_img[1, :, :])
        translate_x = random.uniform(max_minus_translate_x, max_plus_translate_x)
        translate_y = random.uniform(max_minus_translate_y, max_plus_translate_y)
        
        joints_img[0, :, :] = joints_img[0, :, :] + translate_x
        joints_img[1, :, :] = joints_img[1, :, :] + translate_y
        joints_img[0, :, :][orig_img[0, :, :] == 0] = 0
        joints_img[1, :, :][orig_img[1, :, :] == 0] = 0
        
        return joints_img      

    def normalize_joints_coords(self, joints_img):
        orig_img = np.copy(joints_img)

        curr_min_x = np.min(joints_img[0, :, :][joints_img[0, :, :] > 0])
        curr_min_y = np.min(joints_img[1, :, :][joints_img[1, :, :] > 0])

        joints_img[0, :, :] = joints_img[0, :, :] - curr_min_x
        joints_img[1, :, :] = joints_img[1, :, :] - curr_min_y

        max_factor_x = 1 / np.max(joints_img[0, :, :])
        max_factor_y = 1 / np.max(joints_img[1, :, :])

        joints_img[0, :, :] = joints_img[0, :, :] * max_factor_x
        joints_img[1, :, :] = joints_img[1, :, :] * max_factor_y
        joints_img[0, :, :][orig_img[0, :, :] == 0] = 0
        joints_img[1, :, :][orig_img[1, :, :] == 0] = 0

        return joints_img

    def clean_and_scale(self, data_point):
        joints_img = self.remove_joints_outside_img(data_point['X'])
        joints_img = self.scale_joints_coords(joints_img)
        joints_img = self.translate_joints_coords(joints_img)
        joints_img = self.normalize_joints_coords(joints_img)

        data_point['X'] = joints_img
        return data_point

    def batch(self, dataset, train=True):
        set_len = len(dataset)

        x_set = [data['X'].transpose() for data in dataset]
        y_set = [data['Y'] for data in dataset]

        batched_ds = tf.data.Dataset.from_tensor_slices(
            (x_set, y_set))
            
        if train:
            batched_ds = batched_ds.shuffle(set_len)

        return batched_ds.batch(self.batch_size)