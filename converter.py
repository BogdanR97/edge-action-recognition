import tensorflow as tf
import argparse

parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument(
        '--model', help='Path to the saved model directory', required=True)
parser.add_argument(
        '--save_path', help='Path to the saved TFlite model directory', required=True)

args = parser.parse_args()

converter = tf.lite.TFLiteConverter.from_saved_model(args.model)
tflite_model = converter.convert()
open(args.save_path, "wb").write(tflite_model)