import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import tensorflow as tf

class Model():

    LEARNING_RATE = 0.05
    MOMENTUM = 0.9
    REG_LAMBDA = 5e-4

    INPUT_SHAPE = (32, 13, 3)

    KERNEL_SIZE = (3, 3)
    
    NUM_EPOCHS = 130

    def __init__(self, lr_scheduler, actions_no):

        self.actions_no = actions_no

        self.model = tf.keras.Sequential([
            tf.keras.layers.Conv2D(64, kernel_size=self.KERNEL_SIZE,
                strides=1, padding='same',
                activation='relu',
                kernel_regularizer=tf.keras.regularizers.l2(self.REG_LAMBDA),
                input_shape=self.INPUT_SHAPE),
            tf.keras.layers.BatchNormalization(),

            tf.keras.layers.Conv2D(64, kernel_size=self.KERNEL_SIZE,
                strides=1, padding='same',
                activation='relu',
                kernel_regularizer=tf.keras.regularizers.l2(self.REG_LAMBDA)),
            tf.keras.layers.BatchNormalization(),

            tf.keras.layers.MaxPool2D(pool_size=2, strides=2),

            tf.keras.layers.Conv2D(128, kernel_size=self.KERNEL_SIZE,
                strides=1, padding='same',
                activation='relu',
                kernel_regularizer=tf.keras.regularizers.l2(self.REG_LAMBDA)),
            tf.keras.layers.BatchNormalization(),

            tf.keras.layers.Conv2D(128, kernel_size=self.KERNEL_SIZE,
                strides=1, padding='same',
                activation='relu',
                kernel_regularizer=tf.keras.regularizers.l2(self.REG_LAMBDA)),
            tf.keras.layers.BatchNormalization(),

            tf.keras.layers.MaxPool2D(pool_size=2, strides=2),

            tf.keras.layers.Conv2D(256, kernel_size=self.KERNEL_SIZE,
                strides=1, padding='same',
                activation='relu',
                kernel_regularizer=tf.keras.regularizers.l2(self.REG_LAMBDA)),
            tf.keras.layers.BatchNormalization(),

            tf.keras.layers.Conv2D(256, kernel_size=self.KERNEL_SIZE,
                strides=1, padding='same',
                activation='relu',
                kernel_regularizer=tf.keras.regularizers.l2(self.REG_LAMBDA)),
            tf.keras.layers.BatchNormalization(),

            tf.keras.layers.GlobalAveragePooling2D(),
            tf.keras.layers.Dense(self.actions_no, activation='softmax')
        ])

        self.loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
            from_logits=True)

        self.optimizer = tf.keras.optimizers.SGD(
            learning_rate=self.LEARNING_RATE, 
            momentum=self.MOMENTUM)

        self.lr_scheduler = lr_scheduler

    def loss(self, x, y, training):
        y_ = self.model(x, training=training)
        return self.loss_object(y_true=y, y_pred=y_)

    def grad(self, inputs, targets):
        with tf.GradientTape() as tape:
            loss_value = self.loss(inputs, targets, training=True)
        return loss_value, tape.gradient(loss_value, self.model.trainable_variables)

    def early_stop_callback(self):
        pass

    def fit(self, train_ds, test_ds):

        train_loss_results = []
        train_accuracy_results = []

        test_loss_results = []
        test_accuracy_results = []

        if not hasattr(self.optimizer, 'lr'):
            raise ValueError('Optimizer must have a "lr" attribute.')

        for epoch in range(self.NUM_EPOCHS):
            epoch_loss_avg = tf.keras.metrics.Mean()
            epoch_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()

            # Updating the learning rate for the current epoch
            lr = float(tf.keras.backend.get_value(self.optimizer.lr))
            scheduled_lr = self.lr_scheduler(epoch, lr)
            tf.keras.backend.set_value(self.optimizer.lr, scheduled_lr)

            print('\nEpoch %03d: Learning rate is %6.6f.' % (epoch, scheduled_lr))

            # Training loop
            for x, y in train_ds:
                # Optimize the model
                loss_value, grads = self.grad(x, y)
                self.optimizer.apply_gradients(zip(grads, self.model.trainable_variables))

                # Track progress
                epoch_loss_avg.update_state(loss_value) 
                epoch_accuracy.update_state(y, self.model(x, training=True))

            # End epoch
            train_loss_results.append(epoch_loss_avg.result())
            train_accuracy_results.append(epoch_accuracy.result())

            print("Train: Loss: {:.3f}, Accuracy: {:.3%}".format(
                                                                epoch_loss_avg.result(),
                                                                epoch_accuracy.result()))

            # Test set accuracy at the end of epoch
            epoch_test_loss = tf.keras.metrics.Mean()
            epoch_test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()

            for (x, y) in test_ds:
                loss_value, grads = self.grad(x, y)
                logits = self.model(x, training=False)

                epoch_test_loss.update_state(loss_value)
                epoch_test_accuracy.update_state(y, logits)

            test_loss_results.append(epoch_test_loss.result())
            test_accuracy_results.append(epoch_test_accuracy.result())

            print("Test: Loss: {:.3f}, Accuracy: {:.3%}".format(
                                        epoch_test_loss.result(),
                                        epoch_test_accuracy.result()))

        return train_accuracy_results[-1], test_accuracy_results[-1]
    
    def save(self, path):
        self.model.save(path)
