import numpy as np
import scipy.io
import random
import os
import re

class DataFetcher():

    SPLIT_DIR_NAME = "splits"

    SPLIT1_RE = r".*_test_split1.txt"
    SPLIT2_RE = r".*_test_split2.txt"
    SPLIT3_RE = r".*_test_split3.txt"

    ACTION_CAPTURE = r"(.*)_test_split[0-9]+.txt"

    ACTION_FRAMES = 32
    ACTIONS_FILE = "actions.txt"

    JP_FILE = "joint_positions.mat"
    JOINTS_NO = 13

    def __init__(self, joints_dir):
        self.joints_dir = joints_dir
        self.split_dir = os.path.join(joints_dir, self.SPLIT_DIR_NAME)

        with os.scandir(self.split_dir) as entries:
            split_files = [entry.name for entry in entries if entry.is_file()]
            split_1 = [f for f in split_files if re.search(self.SPLIT1_RE, f)]
            split_2 = [f for f in split_files if re.search(self.SPLIT2_RE, f)]
            split_3 = [f for f in split_files if re.search(self.SPLIT3_RE, f)]
            self.split_files = [split_1, split_2, split_3]

        with open(os.path.join(joints_dir, self.ACTIONS_FILE)) as f:
            temp = f.read()
            actions = temp.split('\n')
            self.actions = [action for action in actions if action != '']

            print("There are " + str(len(self.actions)) + " actions in the dataset:")
            print(*self.actions, sep = ", ")
            print("")
    
    def get_actions_no(self):
        return len(self.actions)

    def extract_action(self, split_file):
        return re.findall(self.ACTION_CAPTURE, split_file)[0]

    def get_mat_data(self, action, video_name):
        p = os.path.join(self.joints_dir, action, video_name, self.JP_FILE)
        return scipy.io.loadmat(p)

    def pack_frames(self, mat):
        packages = []
        
        total_frames = mat["pos_img"].shape[2]

        blue_channel = np.zeros((1, self.JOINTS_NO, self.ACTION_FRAMES))

        # If the number of frames in a video exceeds the ACTION_FRAMES value,
        # the first frame from the current package is cut off and the following
        # frame is added at the end of the package. The process is recursive untill
        # all the frames have been consumed.

        if total_frames >= self.ACTION_FRAMES:
            pack_no = total_frames - self.ACTION_FRAMES + 1
            for i in range(pack_no):
                package = mat["pos_img"][:, 2:, i:(self.ACTION_FRAMES + i)]
                package = np.vstack((package, blue_channel))
                packages.append(package)
        
        # If the number of frames in a video does not reach the ACTION_FRAMES limit,
        # random frames will be repeated "ACTION_FRAMES - total_frames" times.
        else:
            package = mat["pos_img"][:, 2:, :]

            rep_frames = self.ACTION_FRAMES - total_frames
            if rep_frames > total_frames:
                rand_frames_list = random.sample(range(total_frames), total_frames)
                rand_frames_list.extend(random.sample(range(total_frames), rep_frames - total_frames))
            else:
                rand_frames_list = random.sample(range(total_frames), rep_frames)
                
            rand_frames_list.sort(reverse=True)

            for frame_id in rand_frames_list:
                rand_frame = package[:, :, frame_id]
                package = np.insert(package, frame_id, rand_frame, axis=2)

            package = np.vstack((package, blue_channel))
            packages.append(package)
            
        return packages
    
    def get_split(self, split_no):
        train_set = []
        train_set_video_count = 0

        validation_set = []
        validation_set_video_count = 0
    
        split_no -= 1

        for action_split in self.split_files[split_no]:

            action = self.extract_action(action_split)
            if action not in self.actions:
                continue

            with open(os.path.join(self.split_dir, action_split)) as f:
                f_content = f.readlines()

                for line in f_content:
                    line = line.strip()
                    if not line:
                        continue
                    
                    video_name, split_id = line.split(' ')
                    video_name = video_name[:-4]
                    split_id = int(split_id)

                    mat = self.get_mat_data(action, video_name)

                    package = self.pack_frames(mat)
                    action_id = self.actions.index(action)
                    package_tuple = list(map((lambda x: {
                                                        'X': x,
                                                        'Y': action_id
                                                    }), package))

                    # TODO: Use tf.Dataset for caching
                    if split_id == 1:
                        train_set.extend(package_tuple)
                        train_set_video_count += 1
                    elif split_id == 2:
                        validation_set.extend(package_tuple)
                        validation_set_video_count += 1
        
        print("Number of training videos: " + str(train_set_video_count))
        print("Number of test videos: " + str(validation_set_video_count))

        return train_set, validation_set